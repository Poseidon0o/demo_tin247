<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
class ArticleController extends Controller
{
    //
    public function show($id)
    {
    	$Article= Article::find($id);
    	if ($Article == "") {
    		return view("error");
    	} else {
    	 return view('Article',['Article'=>$Article]);
    	}
    }
}
