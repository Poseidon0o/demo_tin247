@extends('layouts.app')

@section('content')

<style type="text/css">
  	.pagination li {
   		list-style: none;
   		float: left;
   		margin-left: 5px;
  	}
  	img.cangiua {display: block; margin-left: auto; margin-right: auto;}
 </style>
<div id="main">

	<!-- Featured Post -->
	<article class="post featured">
		<header class="major">
			<span class="date">April 25, 2017</span>
			<h2><a href="#">Articles</a></h2>
		</header>
		<a href="#" class="image main"><img src="{{asset('images/pic01.jpg')}}" alt="" /></a>
		<ul class="actions">
				<li><a href="#" class="button big">Full Story</a></li>
		</ul>
	</article>

	<!-- Posts -->
	<section class="posts">
	@foreach($Article as $list)
		<article>
			<header>
				<span class="date">{{$list->created_at}}</span>
				<h2><a href="show/{{$list->id}}" style="color: black">{{$list->title}}</a></h2>
			</header>
			<a href="show/{{$list->id}}" style="color: black " class="image main"><img src="images/pic02.jpg" c " alt="" /></a>
			<p>{{$list->excerpt}}</p>
			<ul class="actions">
				<li><a href="show/{{$list->id}}" style="color: black;	" class="button" >Full</a></li>
			</ul>
		</article>
	@endforeach
	</section>
	{!! $Article->links() !!}


@endsection




