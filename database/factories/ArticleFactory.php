<?php

$factory->define(App\Article::class, function (Faker\Generator $faker) {
    return [
    	'id_category'=>$faker->numberBetween($min=1,$max=5),
        'title' => $faker->name,
        'excerpt' => $faker-> sentence(20),
        'description' => $faker-> sentence(20),
        
    ];
});
