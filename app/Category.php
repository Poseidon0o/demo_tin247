<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table ='table_categories';
    public function article(){
    	return $this->hasMany('App\Article','id_category','id');
    }
}
