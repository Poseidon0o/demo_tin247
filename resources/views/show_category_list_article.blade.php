<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	{{$Category->id}}
	<div id="page-wrapper">
		<div>
			<div>
				<div>
					
					<h1>Article
						<small>List</small>
					</h1>
				</div>
				<table border="1">
					<thead>
						<tr align="center">
							<td>Number</td>
							<td>name</td>
							<td>Category</td>
							<td>excerpt</td>
							<td>description</td>
						</tr>
					</thead>
					<tbody>
						<?php $i=1 ?>
						@foreach($Article as $al)
						@if($al->id_category == $Category->id)
							<tr  align="center">
								<td>{{$i}}</td>
								<?php $i=$i+1 ?>
								<td>{{$al->title}}</td>
								<td>{{$al->category->name}}</td>
								<td>{{$al->excerpt}}</td>
								<td>{{$al->description}}</td>
								<td><a href="show/{{$al->id}}">show</a></td>
							</tr>
						@endif
						
						@endforeach
					</tbody>
				</table>
			</div>
			
		</div>
	</div>
</body>
</html>
