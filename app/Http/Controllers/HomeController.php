<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Article;
class HomeController extends Controller
{
    //
    // public function index(){
    // 	$Article = Article::all();
    // 	$Category =Category::all();
    // 	return view('home',['Category'=>$Category,'Article'=>$Article]);
    // }
    public function index(){
    	$Article = Article::paginate(10);
    	return view('home',['Article'=>$Article]);
    }
    // public function getarticle($id){
    // 	$Article = Article::all();
    // 	$Category =Category::find($id);
    // 	if ($Category == "") {
    // 		return view("error");
    // 	}
    // 	return view('show_category_list_article',['Category'=>$Category,'Article'=>$Article]);
    // }
}
