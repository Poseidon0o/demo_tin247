
@extends('layouts.app')

@section('content')


<!DOCTYPE HTML>
<!--
	Massively by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Generic Page - Massively by HTML5 UP</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="{{asset('assets/css/main.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/css/noscript.css')}}" />
	</head>
	<body class="is-loading">

		<!-- Wrapper -->
		<div id="wrapper">

			<nav id="nav">

				<ul class="links">
					<li><a href="index.html">This is Massively</a></li>
				</ul>
							
			</nav>

				
			<div id="main">

				<section class="post">

					<header class="major">
						<span class="date">{{$Article->created_at}}</span>
						<p>{{$Article->excerpt}}</p>
					</header>

					<p>{{$Article->description}}</p>

				</section>

			</div>
			
	</body>
</html>

@endsection