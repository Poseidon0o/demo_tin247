<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/','HomeController@index');

// Route::get('test',function(){
// 	$Category = Category::find(1);
// 	foreach ($Category as $key => $value) {
// 		# code...
// 	}
// });
// Route::get('/',function(){
// 	return view('layouts/app');
// });
// Route::get('showarticle/{id}','HomeController@getarticle');
Route::get('show/{id}','ArticleController@show');
// Route::get('showarticle/show/{id}','ArticleController@show');
